# Widget Factory

## DigitalOcean
Access a live deployement of the widget factory here: http://159.65.161.1:8080/

## Setup
Go get the repository:  
`go get gitlab.com/MattL922/widgetfactory`  
Install the widget factory server (you may need to `go get` some packages):  
`go install gitlab.com/MattL922/widgetfactory/cmd/server`  
Change directory into the widgetfactory source directory (the server searches for web assets relative to the source's directory):  
`cd $GO_HOME/src/gitlab.com/MattL922/widgetfactory`  
Run the widget factory server:  
`$GO_HOME/bin/server`  
Navigate to http://localhost:8080/ in your browser

This implementation uses a Sqlite database for simplicity. A file named widgetfactory.sqlite will be created in the working directory when the server is run. This file can be deleted safely when the server is not running (the server will initialize a new one on startup). In a real world implementation, Postgres or MySql would be used.

Due to time constraints, some details are missing such as unit tests, a more polished UI, more advanced data validation, and display of error messages on the UI when bad requests are made.
