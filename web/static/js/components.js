// service provides an API for communicating with the backend services.
const service = {
    // allOrders retrieves all orders from the backend
    allOrders: function() {
        const url = "/v1/orders";
        return fetch(url)
            .then(res => res.json())
            .then(data => data.orders)
            .catch(err => console.log("Failed to get all orders: " + err));
    },
    // createOrder saves a new order with the given widgets
    createOrder: function(widgets) {
        const url = "/v1/orders/";
        const body = JSON.stringify({widgets: widgets});
        return fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: body
        })
        .then(res => res.json())
        .catch(err => console.log("Failed to create order: " + err));
    },
    // deleteOrder deletes an order with the given id
    deleteOrder: function(orderId) {
        const url = `/v1/orders/${orderId}`;
        return fetch(url, {
            method: "DELETE"
        })
        .then(res => res.text())
        .catch(err => console.log("Failed to delete order: " + err));
    },
    // createWidget saves a new widget with the given category, finish, quantity, and size
    createWidget: function(category, finish, quantity, size) {
        const url = "/v1/widgets/";
        const body = JSON.stringify({category: category, finish: finish, quantity: new Number(quantity), size: size});
        return fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: body
        })
        .then(res => res.json())
        // TODO: do something more useful here
        .catch(err => console.log("Failed to create widget: " + err));
    },
    // searchWidgets retrieves a list of widgets based on the given filters
    searchWidgets: function(category, finish, size) {
        const url = `/v1/widgets/?category=${category}&finish=${finish}&size=${size}`;
        return fetch(url)
            .then(res => res.json())
            .then(data => data.widgets)
            // TODO: do something more useful here
            .catch(err => console.log("Failed to search widgets: " + err));
    },
    // updateWidgetQuantity updates a widget with a new quantity
    updateWidgetQuantity: function(widgetId, qty) {
        const url = `/v1/widgets/${widgetId}/quantity/${qty}`;
        return fetch(url, {
            method: "PUT"
        })
        .then(res => res.text())
        // TODO: do something more useful here
        .catch(err => console.log("Failed to create widget: " + err));
    }
};

// TODO: don't hardcode these
const CATEGORIES = ["Widget Basic", "Widget Prime", "Widget Extreme"];
const FINISHES = ["Chrome", "Gold", "Silver", "Bronze"];
const SIZES = ["Small", "Medium", "Large"];

// FilterableWidgetTable displays a table of widgets that can be filtered and
// added to a shopping cart.
class FilterableWidgetTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            category: "",
            finish: "",
            size: "",
            widgets: [],
            cartItems: new Map()
        };
        this.onCategoryChange = this.onCategoryChange.bind(this);
        this.onFinishChange = this.onFinishChange.bind(this);
        this.onSizeChange = this.onSizeChange.bind(this);
        this.props.service.searchWidgets("", "", "").then(data => this.setState({widgets: data}));
        this.addToCart = this.addToCart.bind(this);
        this.removeFromCart = this.removeFromCart.bind(this);
        this.placeOrder = this.placeOrder.bind(this);
    }

    onCategoryChange(category) {
        const finish = this.state.finish;
        const size = this.state.size;
        this.props.service.searchWidgets(category, finish, size).then(data => this.setState({category: category, widgets: data}));
    }

    onFinishChange(finish) {
        const category = this.state.category;
        const size = this.state.size;
        this.props.service.searchWidgets(category, finish, size).then(data => this.setState({finish: finish, widgets: data}));
    }

    onSizeChange(size) {
        const category = this.state.category;
        const finish = this.state.finish;
        this.props.service.searchWidgets(category, finish, size).then(data => this.setState({size: size, widgets: data}));
    }

    addToCart(widget) {
        this.setState((state) => {
            const cartItems = state.cartItems;
            if(cartItems.has(widget.id)) {
                const item = cartItems.get(widget.id);
                item.quantity += 1;
                cartItems.set(item.id, item);
            } else {
                cartItems.set(widget.id, {id: widget.id, category: widget.category, finish: widget.finish, quantity: 1, size: widget.size});
            }
            return {cartItems: cartItems};
        });
    }

    removeFromCart(widgetId) {
        this.setState((state) => {
            const cartItems = state.cartItems;
            cartItems.delete(widgetId);
            return {cartItems: cartItems};
        });
    }

    placeOrder() {
        this.props.service.createOrder(Array.from(this.state.cartItems.values())).then(data => {
            console.log("order placed: " + JSON.stringify(data));
            this.props.service.searchWidgets("", "", "").then(data => this.setState({widgets: data}));
            this.setState({cartItems: new Map()});
        });
    }

    render() {
        return (
            <div>
                <h4>Shop</h4>
                <WidgetFilters
                    categories={CATEGORIES}
                    category={this.state.category}
                    finishes={FINISHES}
                    finish={this.state.finish}
                    sizes={SIZES}
                    size={this.state.size}
                    onCategoryChange={this.onCategoryChange}
                    onFinishChange={this.onFinishChange}
                    onSizeChange={this.onSizeChange} />
                <WidgetTable data={this.state.widgets} addToCart={this.addToCart} />
                <WidgetCart items={Array.from(this.state.cartItems.values())} removeFromCart={this.removeFromCart} placeOrder={this.placeOrder} />
            </div>
        );
    }
}

// WidgetFilters displays form controls that can filter a table of widgets
// by category, finish, and size.
class WidgetFilters extends React.Component {
    constructor(props) {
        super(props);
        this.onCategoryChange = this.onCategoryChange.bind(this);
        this.onFinishChange = this.onFinishChange.bind(this);
        this.onSizeChange = this.onSizeChange.bind(this);
    }

    onCategoryChange(e) {
        this.props.onCategoryChange(e.target.value);
    }

    onFinishChange(e) {
        this.props.onFinishChange(e.target.value);
    }

    onSizeChange(e) {
        this.props.onSizeChange(e.target.value);
    }

    render() {
        return (
            <form className="form-inline">
                <label htmlFor="category-filter">Category:</label>
                <select id="category-filter" className="form-control form-control-sm" value={this.props.category} onChange={this.onCategoryChange}>
                    <option value="">Any</option>
                    {this.props.categories.map(category => <option key={category} value={category}>{category}</option>)}
                </select>
                <label htmlFor="finish-filter">Finish:</label>
                <select id="finish-filter" className="form-control form-control-sm" value={this.props.finish} onChange={this.onFinishChange}>
                    <option value="">Any</option>
                    {this.props.finishes.map(finish => <option key={finish} value={finish}>{finish}</option>)}
                </select>
                <label htmlFor="size-filter">Size:</label>
                <select id="size-filter" className="form-control form-control-sm" value={this.props.size} onChange={this.onSizeChange}>
                    <option value="">Any</option>
                    {this.props.sizes.map(size => <option key={size} value={size}>{size}</option>)}
                </select>
            </form>
        );
    }
}

// WidgetTable displays a table of widgets.
class WidgetTable extends React.Component {
    render() {
        return (
            <table className="table table-sm">
                <thead className="thead-light">
                    <tr>
                        <th>Category</th>
                        <th>Finish</th>
                        <th>Size</th>
                        <th>Quantity</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.data.map(widget => <WidgetRow key={widget.id} widget={widget} addToCart={this.props.addToCart} />)}
                </tbody>
            </table>
        );
    }
}

// WidgetInventory displays all of the widgets that are currently in the
// inventory. It also provides the ability to add new widgets to the inventory.
class WidgetInventory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data
        }
        this.createWidget = this.createWidget.bind(this);
        this.onQtyChange = this.onQtyChange.bind(this);
        this.saveInventory = this.saveInventory.bind(this);
    }

    createWidget(category, finish, quantity, size) {
        this.props.service.createWidget(category, finish, quantity, size).then(() => {
            this.props.service.searchWidgets("", "", "").then(data => this.setState({data: data}));
        });
    }

    onQtyChange(widgetId, qty) {
        // TODO: do this more efficiently - maybe use a Map
        this.setState(state => {
            const newData = state.data.map(d => {
                if(d.id == widgetId) d.quantity = qty;
                return d;
            });
            return {data: newData};
        });
    }

    saveInventory() {
        // TODO: make a bulk update API method so these don't have to be updated one at a time
        this.state.data.map(widget => {
            this.props.service.updateWidgetQuantity(widget.id, widget.quantity);
        });
    }

    render() {
        return (
            <div>
                <h4>Inventory</h4>
                <WidgetInventoryTable data={this.state.data} onQtyChange={this.onQtyChange} />
                <button type="button" className="btn btn-primary" onClick={this.saveInventory}>Update Inventory</button>
                <div style={{height:"2rem"}}></div>
                <h4>Add Widget</h4>
                <WidgetForm
                    categories={CATEGORIES}
                    finishes={FINISHES}
                    sizes={SIZES}
                    onSubmit={this.createWidget} />
            </div>
        );
    }
}

// WidgetForm displays a form that allows the addition of a new widget to the
// inventory.
class WidgetForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            category: "",
            finish: "",
            quantity: "0",
            size: ""
        };
        this.onCategoryChange = this.onCategoryChange.bind(this);
        this.onFinishChange = this.onFinishChange.bind(this);
        this.onQuantityChange = this.onQuantityChange.bind(this);
        this.onSizeChange = this.onSizeChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    clear() {
        this.setState({category: "", finish: "", quantity: "0", size: ""});
    }

    onCategoryChange(e) {
        this.setState({category: e.target.value});
    }

    onFinishChange(e) {
        this.setState({finish: e.target.value});
    }

    onQuantityChange(e) {
        this.setState({quantity: e.target.value});
    }

    onSizeChange(e) {
        this.setState({size: e.target.value});
    }

    onSubmit() {
        const category = this.state.category;
        const finish = this.state.finish;
        const quantity = this.state.quantity;
        const size = this.state.size;
        this.props.onSubmit(category, finish, quantity, size);
        this.clear()
    }

    render() {
        return (  
            <form>
                <div className="form-group">
                    <label htmlFor="select-category">Category:</label>
                    <select id="select-category" className="form-control form-control-sm" value={this.state.category} onChange={this.onCategoryChange} name="category" required>
                        <option value="">Choose...</option>
                        {this.props.categories.map(category => <option key={category} value={category}>{category}</option>)}
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="select-finish">Finish:</label>
                    <select id="select-finish" className="form-control form-control-sm" value={this.state.finish} onChange={this.onFinishChange} name="finish" required>
                        <option value="">Choose...</option>
                        {this.props.finishes.map(finish => <option key={finish} value={finish}>{finish}</option>)}
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="select-size">Size:</label>
                    <select id="select-size" className="form-control form-control-sm" value={this.state.size} onChange={this.onSizeChange} name="size" required>
                        <option value="">Choose...</option>
                        {this.props.sizes.map(size => <option key={size} value={size}>{size}</option>)}
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="select-quantity">Quantity:</label>
                    <input type="number" min="0" value={this.state.quantity} required id="select-quantity" className="form-control form-control-sm" name="quantity" onChange={this.onQuantityChange} />
                </div>
                <button type="button" className="btn btn-primary" onClick={this.onSubmit}>Submit</button>
            </form>
        );
    }
}

// WidgetRow displays a table row in the widget shop.
class WidgetRow extends React.Component {
    constructor(props) {
        super(props);
        this.addToCart = this.addToCart.bind(this);
    }

    addToCart() {
        this.props.addToCart(this.props.widget);
    }

    render() {
        const widget = this.props.widget;
        return (
            <tr>
                <td>{widget.category}</td>
                <td>{widget.finish}</td>
                <td>{widget.size}</td>
                <td>{widget.quantity}</td>
                <td><button className="btn btn-outline-primary btn-sm" onClick={this.addToCart}>Add to cart</button></td>
            </tr>
        );
    }
}

// WidgetCart displays a list of widgets that are currently in the user's
// cart.
class WidgetCart extends React.Component {
    render() {
        const items = this.props.items;
        return (
            <div>
                <h4>Your Cart</h4>
                <ul>
                    {items.map(item => <WidgetCartItem key={item.id} item={item} removeFromCart={this.props.removeFromCart} />)}
                </ul>
                <button type="button" className="btn btn-primary" onClick={this.props.placeOrder}>Place Order</button>
            </div>
        );
    }
}

// WidgetCartItem displays a single item in the widget shopping cart.
class WidgetCartItem extends React.Component {
    constructor(props) {
        super(props);
        this.removeFromCart = this.removeFromCart.bind(this);
    }

    removeFromCart() {
        this.props.removeFromCart(this.props.item.id);
    }

    render() {
        const item = this.props.item;
        return (
            <li>
                <b>{item.category}</b> <small><b>Finish:</b>{item.finish} <b>Size:</b>{item.size}</small> x{item.quantity} <button type="button" className="btn btn-sm btn-link" onClick={this.removeFromCart}>Remove</button>
            </li>
        );
    }
}

// WidgetInventoryTable displays a table of widgets that are currently in the
// inventory. Widget quantites can be edited in place in this table.
class WidgetInventoryTable extends React.Component {
    render() {
        return (
            <table className="table table-sm">
                <thead className="thead-light">
                    <tr>
                        <th>Category</th>
                        <th>Finish</th>
                        <th>Size</th>
                        <th>Quantity</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.data.map(widget => <WidgetInventoryRow key={widget.id} widget={widget} onQtyChange={this.props.onQtyChange} />)}
                </tbody>
            </table>
        );
    }
}

// WidgetInventoryRow displays a single widget in the inventory table.
class WidgetInventoryRow extends React.Component {
    constructor(props) {
        super(props);
        this.onQtyChange = this.onQtyChange.bind(this);
    }

    onQtyChange(e) {
        const widget = this.props.widget;
        const newQty = new Number(e.target.value);
        this.props.onQtyChange(widget.id, newQty);
    }

    render() {
        const widget = this.props.widget;
        return (
            <tr>
                <td>{widget.category}</td>
                <td>{widget.finish}</td>
                <td>{widget.size}</td>
                <td><input type="number" value={widget.quantity} onChange={this.onQtyChange} /></td>
            </tr>
        );
    }
}

// OrderTable displays a list of orders that have been placed in the widget
// store.
class OrderTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: this.props.data
        }
        this.onDelete = this.onDelete.bind(this);
    }

    onDelete(orderId) {
        this.props.service.deleteOrder(orderId).then(() => {
            this.setState(state => ({orders: state.orders.filter(order => order.id != orderId)}));
        });
    }

    render() {
        const orders = this.state.orders;
        return (
            <div>
                <h4>All Orders</h4>
                <table className="table table-sm">
                    <thead className="thead-light">
                        <tr>
                            <th>Order ID</th>
                            <th>Widgets</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {orders.map(order => <OrderRow key={order.id} order={order} onDelete={this.onDelete} />)}
                    </tbody>
                </table>
            </div>
        );
    }
}

// OrderRow displays a single order in the order table.
class OrderRow extends React.Component {
    constructor(props) {
        super(props);
        this.onDelete = this.onDelete.bind(this);
    }

    onDelete() {
        this.props.onDelete(this.props.order.id);
    }

    render() {
        const order = this.props.order;
        return (
            <tr>
                <td>{order.id}</td>
                <td>
                    {order.widgets.map(widget => <div key={widget.id}><b>{widget.category}</b> <small><b>Finish:</b>{widget.finish} <b>Size:</b>{widget.size}</small> x{widget.quantity}</div>)}
                </td>
                <td><button type="button" className="btn btn-sm btn-outline-danger" onClick={this.onDelete}>Delete</button></td>
            </tr>
        );
    }
}
