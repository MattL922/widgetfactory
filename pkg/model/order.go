package model

// Order is a collection of Widgets that have been purchased from the widget
// store.
type Order struct {
    Id string `json:"id"`
    Widgets []Widget `json:"widgets"`
}
