package model

// Widget is an item that can be purchased in the widget store
type Widget struct {
    Category string `json:"category"`
    Finish string `json:"finish"`
    Id string `json:"id"`
    Quantity uint `json:"quantity"`
    Size string `json:"size"`
}
