package store

import (
    "errors"
    "widgetfactory/pkg/model"
)

var ErrInsufficientQuantity = errors.New("Insuffient widget quantity")

// WidgetStore provides an interface for interacting with Widget related data.
type WidgetStore interface {
    // CreateWidget saves the provided Widget in the backing database.
    CreateWidget(model.Widget) error
    // SearchWidgets retrieves a list of Widgets based on the given category,
    // finish, and size filters.
    SearchWidgets(category, finish, size string) ([]model.Widget, error)
    // UpdateWidgetQuantity updates the quantity of a Widget by ID.
    UpdateWidgetQuantity(widgetId string, qty int) error
}

