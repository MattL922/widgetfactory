package sqlite

import (
    "database/sql"
    "errors"
    _ "github.com/mattn/go-sqlite3"
    "os"
    "os/signal"
    "sync"
    "widgetfactory/pkg/model"
    "widgetfactory/pkg/store"
)

// Sqlite interfaces with a Sqlite file and implements the OrderStore and
// WidgetStore interfaces.
type Sqlite struct {
    *sql.DB
    mu sync.RWMutex
}

// New creates and initializes a new Sqlite database.
func New(file string) (Sqlite, error) {
    var sqlite Sqlite

    // get a handle to the sqlite db
    db, err := sql.Open("sqlite3", file)
    if err != nil { return sqlite, err }

    // check that we are connected
    if err := db.Ping(); err != nil { return sqlite, err }

    // cleanup db resources on interrupt signal
    c := make(chan os.Signal, 1)
    signal.Notify(c, os.Interrupt)
    go func() {
        <-c
        db.Close()
        os.Exit(1)
    }()

    // set up tables and indexes
    sqlite = Sqlite{db, sync.RWMutex{}}
    sqlite.createTables()
    sqlite.createIndexes()

    return sqlite, nil
}

// createTables creates all tables if they do not yet exist.
func (db Sqlite) createTables() error {
    var err error
    _, err = db.Exec(`CREATE TABLE IF NOT EXISTS widgets (
        id TEXT NOT NULL,
        category TEXT NOT NULL,
        finish TEXT NOT NULL,
        quantity INTEGER NOT NULL,
        size TEXT NOT NULL,
        PRIMARY KEY (id),
        UNIQUE(category, finish, size) ON CONFLICT ABORT)`)
    if err != nil { return err }
    _, err = db.Exec(`CREATE TABLE IF NOT EXISTS orders (
        id TEXT NOT NULL,
        widget_id TEXT NOT NULL REFERENCES widgets(id) ON DELETE CASCADE ON UPDATE CASCADE,
        quantity INTEGER NOT NULL)`)
    if err != nil { return err }
    return nil
}

// createIndexes creates table indexes if they do not yet exist.
func (db Sqlite) createIndexes() {
    // TODO: create table indexes for faster queries
}

func (db Sqlite) AddWidgetQuantity(widgetId string, delta int) error {
    db.mu.Lock()
    defer db.mu.Unlock()
    _, err := db.Exec("UPDATE widgets SET quantity=quantityr+? WHERE id=?", delta, widgetId)
    return err
}

func (db Sqlite) CreateWidget(widget model.Widget) error {
    db.mu.Lock()
    defer db.mu.Unlock()
    _, err := db.Exec("INSERT INTO widgets VALUES (?, ?, ?, ?, ?)", widget.Id, widget.Category, widget.Finish, widget.Quantity, widget.Size)
    return err
}

func (db Sqlite) SearchWidgets(category, finish, size string) ([]model.Widget, error) {
    db.mu.RLock()
    defer db.mu.RUnlock()
    widgets := []model.Widget{}
    rows, err := db.Query("SELECT * FROM widgets WHERE (?='' OR category=?) AND (?='' OR finish=?) AND (?='' OR size=?)", category, category, finish, finish, size, size)
    if err != nil { return widgets, err }
    defer rows.Close()
    for rows.Next() {
        var widget model.Widget
        if err := rows.Scan(&widget.Id, &widget.Category, &widget.Finish, &widget.Quantity, &widget.Size); err != nil {
            return widgets, err
        }
        widgets = append(widgets, widget)
    }
    return widgets, rows.Err()
}

// TODO: rows affected will also be 0 when the widget id does not exist
func (db Sqlite) UpdateWidgetQuantity(widgetId string, qty int) error {
    db.mu.Lock()
    defer db.mu.Unlock()
    if qty < 0 { return errors.New("Widget quantity must be non-negative") }
    _, err := db.Exec("UPDATE widgets SET quantity=? WHERE id=?", qty, widgetId)
    if err != nil { return err }
    return nil
}

func (db Sqlite) AllOrders() ([]model.Order, error) {
    db.mu.RLock()
    defer db.mu.RUnlock()
    orders := []model.Order{}
    ordersById := map[string][]model.Widget{}
    rows, err := db.Query("SELECT o.id, o.quantity, w.id, w.category, w.finish, w.size FROM orders o LEFT JOIN widgets w ON o.widget_id=w.id")
    if err != nil { return orders, err }
    defer rows.Close()
    for rows.Next() {
        var orderId string
        var widget model.Widget
        if err := rows.Scan(&orderId, &widget.Quantity, &widget.Id, &widget.Category, &widget.Finish, &widget.Size); err != nil {
            return orders, err
        }
        ordersById[orderId] = append(ordersById[orderId], widget)
    }
    if err := rows.Err(); err != nil { return orders, err }
    for orderId, widgets := range ordersById {
        orders = append(orders, model.Order{orderId, widgets})
    }
    return orders, nil
}

func (db Sqlite) CreateOrder(order model.Order) error {
    db.mu.Lock()
    defer db.mu.Unlock()
    tx, err := db.Begin()
    if err != nil { return err }

    for _, widget := range order.Widgets {
        // Decrement the widget quantities
        result, err := tx.Exec("UPDATE widgets SET quantity=quantity+? WHERE id=? AND quantity >= ?", -widget.Quantity, widget.Id, widget.Quantity)
        if err != nil {
            if err := tx.Rollback(); err != nil { return err }
            return err
        }
        rows, err := result.RowsAffected()
        if err != nil {
            if err := tx.Rollback(); err != nil { return err }
            return err
        }
        if rows != 1 {
            if err := tx.Rollback(); err != nil { return err }
            return store.ErrInsufficientQuantity
        }

        // Save the widget to the order
        if _, err := tx.Exec("INSERT INTO orders VALUES (?, ?, ?)", order.Id, widget.Id, widget.Quantity); err != nil {
            if err := tx.Rollback(); err != nil { return err }
            return err
        }
    }

    return tx.Commit()
}

func (db Sqlite) DeleteOrder(orderId string) error {
    db.mu.Lock()
    defer db.mu.Unlock()
    // TODO: use a transaction and add the quantities back to the widgets in the order
    _, err := db.Exec("DELETE FROM orders WHERE id=?", orderId)
    return err
}

func (db Sqlite) OrderById(id string) (model.Order, error) {
    return model.Order{}, nil
}
