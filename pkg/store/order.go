package store

import (
    "widgetfactory/pkg/model"
)

// OrderStore provides an interface for interacting with Order related data.
type OrderStore interface {
    // AllOrders retrieves all Orders from the backing database.
    AllOrders() ([]model.Order, error)
    // CreateOrder saves the provided Order in the backing database.
    CreateOrder(model.Order) error
    // DeleteOrder deletes the Order with the given ID.
    DeleteOrder(orderId string) error
    // OrderById retrieves the Order with the given ID.
    OrderById(orderId string) (model.Order, error)
}
