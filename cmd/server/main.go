package main

import (
    "github.com/gin-contrib/multitemplate"
    "github.com/gin-gonic/gin"
    "github.com/satori/go.uuid"
    "log"
    "net/http"
    "strconv"
    "widgetfactory/pkg/model"
    "widgetfactory/pkg/store"
    "widgetfactory/pkg/store/sqlite"
)

func main() {
    // connect to and initialize the db
    db, err := sqlite.New("./widgetfactory.sqlite")
    if err != nil {
        log.Fatalf("Failed to initalize db: %s\n", err.Error())
    }

    server := gin.Default()
    server.HTMLRender = templateRenderer()
    server.StaticFS("/static", http.Dir("./web/static"))

    server.GET("/", func(c *gin.Context) {
        c.HTML(200, "index", nil)
    })
    server.GET("/inventory", func(c *gin.Context) {
        c.HTML(200, "inventory", nil)
    })
    server.GET("/orders", func(c *gin.Context) {
        c.HTML(200, "orders", nil)
    })

    // REST API
    v1 := server.Group("/v1")
    v1orders := v1.Group("/orders")
    v1orders.DELETE("/:id", DeleteOrderHandler(db))
    v1orders.GET("/", AllOrdersHandler(db))
    v1orders.POST("/", CreateOrderHandler(db))
    v1widgets := v1.Group("/widgets")
    v1widgets.GET("/", SearchWidgetsHandler(db))
    v1widgets.POST("/", CreateWidgetHandler(db))
    v1widgets.PUT("/:id/quantity/:qty", UpdateWidgetQuantityHandler(db))

    server.Run(":8080")
}

func templateRenderer() multitemplate.Renderer {
    r := multitemplate.NewRenderer()
    r.AddFromFiles("index", "web/templates/master.tmpl", "web/templates/index.tmpl")
    r.AddFromFiles("inventory", "web/templates/master.tmpl", "web/templates/inventory.tmpl")
    r.AddFromFiles("orders", "web/templates/master.tmpl", "web/templates/orders.tmpl")
    return r
}

func AllOrdersHandler(orderStore store.OrderStore) func(c *gin.Context) {
    return func(c *gin.Context) {
        orders, err := orderStore.AllOrders()
        if err != nil {
            log.Printf("Failed to get all orders: %s\n", err.Error())
            c.String(500, "")
            return
        }

        c.JSON(200, gin.H{"orders": orders})
    }
}

func DeleteOrderHandler(orderStore store.OrderStore) func(c *gin.Context) {
    return func(c *gin.Context) {
        orderId := c.Param("id")

        if err := orderStore.DeleteOrder(orderId); err != nil {
            log.Printf("Failed to delete order: %s\n", err.Error())
            c.String(500, "")
            return
        }

        c.String(200, "")
    }
}

func CreateOrderHandler(orderStore store.OrderStore) func(c *gin.Context) {
    return func(c *gin.Context) {
        var order model.Order
        if err := c.BindJSON(&order); err != nil {
            log.Printf("Failed to bind post data to order: %s\n", err.Error())
            c.String(500, "")
            return
        }

        // TODO: implement validation
        if len(order.Widgets) == 0 {
            log.Printf("Order did not contain any widgets")
            c.String(400, "Empty was empty")
            return
        }

        id, err := uuid.NewV4()
        if err != nil {
            log.Printf("Failed to generate UUID for order: %s\n", err.Error())
            c.String(500, "")
            return
        }

        order.Id = id.String()

        if err := orderStore.CreateOrder(order); err != nil {
            log.Printf("Failed to create order: %s\n", err.Error())
            c.String(500, "")
            return
        }

        c.JSON(200, gin.H{"order": order})
    }
}

func CreateWidgetHandler(widgetStore store.WidgetStore) func(c *gin.Context) {
    return func(c *gin.Context) {
        var widget model.Widget
        if err := c.BindJSON(&widget); err != nil {
            log.Printf("Failed to bind post data to widget: %s\n", err.Error())
            c.String(500, "")
            return
        }

        // TODO: implement better validation
        if widget.Category == "" || widget.Finish == "" || widget.Size == "" {
            log.Printf("Invalid widget %+v\n", widget)
            c.String(400, "Invalid widget")
            return
        }

        id, err := uuid.NewV4()
        if err != nil {
            log.Printf("Failed to generate UUID for widget: %s\n", err.Error())
            c.String(500, "")
            return
        }

        widget.Id = id.String()

        if err := widgetStore.CreateWidget(widget); err != nil {
            log.Printf("Failed to create widget: %s\n", err.Error())
            c.String(500, "")
            return
        }

        c.JSON(200, gin.H{"widget": widget})
    }
}

func SearchWidgetsHandler(widgetStore store.WidgetStore) func(c *gin.Context) {
    return func(c *gin.Context) {
        category := c.Query("category")
        finish := c.Query("finish")
        size := c.Query("size")

        widgets, err := widgetStore.SearchWidgets(category, finish, size)
        if err != nil {
            log.Printf("Failed to search widgets: %s\n", err.Error())
            c.String(500, "")
            return
        }

        c.JSON(200, gin.H{"widgets": widgets})
    }
}

func UpdateWidgetQuantityHandler(widgetStore store.WidgetStore) func(c *gin.Context) {
    return func(c *gin.Context) {
        widgetId := c.Param("id")
        qty, err := strconv.Atoi(c.Param("qty"))
        if err != nil {
            log.Printf("Failed to convert qty to an integer: %s\n", err.Error())
            c.String(400, "")
            return
        }

        if err := widgetStore.UpdateWidgetQuantity(widgetId, qty); err != nil {
            log.Printf("Failed to update widget quantity: %s\n", err.Error())
            c.String(500, "")
            return
        }

        c.String(200, "")
    }
}
